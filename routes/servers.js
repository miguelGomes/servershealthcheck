var express = require('express');
var router = express.Router();
var config = require('./../core/configLoader');

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.render('servers', {
        title: 'List Server Status',
        servers: config.config
    });
});

module.exports = router;
