var config = {
    config: []
};

var fs = require('fs');

var filename = process.argv[3];

fs.readFile(filename, 'utf8', function(err, data) {
    if (err) throw err;
    config.config = JSON.parse(data);
});

module.exports = config;
