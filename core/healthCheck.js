var CronJob = require('cron').CronJob;
var https = require("https");

var debug = require('debug')('healthCheck');

var config = require('./configLoader');

var treatInfo = function(serverInfo) {

    https.get({
        host: serverInfo.host,
        path: serverInfo.path
    }, function (res) {
        if (res.statusCode === 200) {
            serverInfo.status = 'UP';
            serverInfo.lastUpTime = Date();
        } else {
            serverInfo.status = 'DOWN';
        }
        debug(serverInfo.name + ' -> ' + serverInfo.status);
    }, function () {
        serverInfo.status = 'ERROR';
    });
};

new CronJob('*/5 * * * * *', function() {

    debug('Test Servers');

    for (var index = 0; index < config.config.length; index++) {
        treatInfo(config.config[index]);
    }

}, null, true, '');